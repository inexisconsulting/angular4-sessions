import { AuthGuardGuard } from './auth-guard.guard';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers:[AuthGuardGuard]
})
export class AuthModule { }
