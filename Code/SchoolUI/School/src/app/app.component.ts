import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  name = "isuru";
  studentCount: number;
  schoolName: string = "Parent to the child";

  getStudentCount(event) {
    console.log(event)
    this.studentCount = event;
  }
}


