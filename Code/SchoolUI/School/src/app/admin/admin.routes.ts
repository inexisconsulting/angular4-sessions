import { AdmindetailsComponent } from './admindetails/admindetails.component';
import { AdmincreateComponent } from './admincreate/admincreate.component';
import { AdminComponent } from './admin.component';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

export const adminRoutes: Routes = [
    {
        path: "admin",
        component: AdminComponent,
        children: [
            { path: '', component: AdmindetailsComponent },
            { path: 'createAdmin', component: AdmincreateComponent }
        ]
    },


];

export const adminRouting: ModuleWithProviders = RouterModule.forChild(adminRoutes);