import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdmindetailsComponent } from './admindetails/admindetails.component';
import { AdmincreateComponent } from './admincreate/admincreate.component';
import { AdminComponent } from './admin.component';
import { adminRouting } from './admin.routes'

@NgModule({
  imports: [
    CommonModule,
    adminRouting
  ],
  declarations: [AdmindetailsComponent,
    AdmincreateComponent,
    AdminComponent]
})
export class AdminModule { }
