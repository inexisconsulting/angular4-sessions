import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admindetails',
  templateUrl: './admindetails.component.html',
  styleUrls: ['./admindetails.component.css']
})
export class AdmindetailsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  createAdmin() {
    this.router.navigate(["/admin/createAdmin"])
  }
}
