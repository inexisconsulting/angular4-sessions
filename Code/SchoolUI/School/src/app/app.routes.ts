import { AuthGuardGuard } from './auth/auth-guard.guard';
import { StudentsComponent } from './students/students.component';
import { HomeComponent } from './home/home.component';
import { CourseComponent } from './course/course.component';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';


export const routes: Routes = [
    {
        path: "",
        component: HomeComponent
    },
    {
        path: "student",
        component: StudentsComponent,
        canActivate: [AuthGuardGuard]
    },
    {
        path: "course",
        component: CourseComponent
    },
    {
        path: "course/:id",
        component: CourseComponent
    }

];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);