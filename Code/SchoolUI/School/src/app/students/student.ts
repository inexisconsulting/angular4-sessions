export class Student {
    Id: number;
    FirstName: string;
    LastName: string;
    Address: string;
    Age: number;
}