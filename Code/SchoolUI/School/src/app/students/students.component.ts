import { StudentService } from './student.service';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, Output, Input, EventEmitter } from '@angular/core';
import { Student } from './student';


@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  public headingName: string = "Create Student";
  public student: Student = new Student();
  public studentList: Student[] = [];
  @ViewChild('studentForm') studentForm;
  @Output() studentCount = new EventEmitter<number>();
  @Input() schoolNameInput: string;


  constructor(private router: Router, private studentService: StudentService) { }

  ngOnInit() {
    console.log(this.schoolNameInput)
  }

  addStudent(): void {


    if (this.studentForm.invalid) {

      return;
    }

    this.studentService.saveStudent(this.student).subscribe(response => { console.log(response) },
      error => {
        console.log(error)
      })

    let st = Object.assign({}, this.student);
    this.studentList.push(st);

    this.studentCount.emit(this.studentList.length)

    console.log(this.studentList)

  }

  goToCourses() {
    this.router.navigate(["/course"])
  }

  goToCourse() {
    let id = 1;
    this.router.navigate(["/course", id])
  }



}
