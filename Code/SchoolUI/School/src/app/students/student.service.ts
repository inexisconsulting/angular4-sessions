import { Student } from './student';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/throw';

@Injectable()
export class StudentService {

  private apiUrl = "http://localhost:9015/api/Students";
  constructor(private http: Http) {

  }

  public saveStudent(student: Student) {

    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(this.apiUrl, JSON.stringify(student), { headers: headers })

  }

}
